# Instruções para o Exemplo - Infrastructe as Code com Ansible

Para o exemplo foram usado serviço da AWS EC2, onde é possível iniciar instâncias linux na nuvem rapidamente para rodar os exemplos.

Para o exemplo abaixo foi usada instâncias no `free tier` (t2.micro), basta iniciar alguma(s) instâncias e adicionar o endereço da instância no arquivo `inventory.yml` na pasta `iac`.

Para ter certeza que você tem acesso a sua instância EC2, você pode acessá-la via SSH através do comando abaixo:

### Acesso ao Ec2 AWS

> ssh -i <path da chave .pem> <aws-ec2-address>

Após validar que tá tudo certo com seu acesso, podemos executar o Ansible Playbook onde irá subir uma servidor web que serve uma página HTML estática, sem precisar realizar nenhuma operação manual que não seja a execução do comando para rodar o playbook.

### Configurando a máquina usando Ansible Playbok:

Na pasta `iac` tem o playbook `app.yml` e o `inventory.yml` que possui o endereço e configuração de acesso das instâncias iniciadas na AWS, como vimos anteriormente.

Com tudo pronto, para configurar as máquinas, basta executar o comando abaixo:

> ansible-playbook -i inventory.yml app.yml

Experimente realizar uma mudança no arquivo `index.html`, executar o playbook novamente e deixa a magia acontecer!
